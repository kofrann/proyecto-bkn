Project Templates
Thanks for considering a contribution to GitLab's built-in project templates.

Prerequisites
For contributing new project templates or updating existing ones, you will need to have wget installed.

For new templates you will also need to ensure you have gtar and jq installed.

If you are using the GitLab Development Kit (GDK) you will have to disable praefect and regenerate the Procfile, as the rake task is not currently compatible with it.

# gitlab-development-kit/gdk.yml
praefect:
  enabled: false
Contributing GitLab Pages templates
If you want to contribute to an existing GitLab Pages template, contribute to the corresponding project on https://gitlab.com/pages.

If you want to add a new template, create a public project and open an issue under https://gitlab.com/gitlab-org/gitlab for this project to be added to https://gitlab.com/pages.

Some example templates currently distributed with GitLab include:

Pages/Gatsby
Pages/Hugo
Pages/Jekyll
Pages/PlainHTML
Pages/GitBook
Ruby on Rails
GitLab cluster management
Android
Go Micro
HIPAA Audit Protocol
Contributing a new project template
If you'd like to contribute a new built-in project template to be distributed with GitLab, please do the following:

Create a new public project with the project content you'd like to contribute in a namespace of your choosing. You can view a working example here.
Projects should be as simple as possible and free of any unnecessary assets or dependencies.
When the project is ready for review, please create a new issue in GitLab with a link to your project.
In your issue, @ mention the relevant Backend Engineering Manager and Product Manager for the Create:Source Code group.
To make the project template available when creating a new project, the vendoring process will have to be completed:

Create a working template (example)
2 types of built-in templates are available within GitLab:
Normal templates: Available in GitLab Core, Starter and above (This is the most common type of built-in template)
To contribute a normal template:
Add details of the template in the localized_templates_table method in gitlab/lib/gitlab/project_template.rb,
Add details of the template in spec/lib/gitlab/project_template_spec.rb, in the test for the all method, and
Add details of the template in gitlab/app/assets/javascripts/projects/default_project_templates.js.
See MR !25318 for an example
Enterprise templates: Introduced in GitLab 12.10, that are available only in GitLab Gold & Ultimate.
To contribute an Enterprise template:
Add details of the template in the localized_ee_templates_table method in gitlab/ee/lib/ee/gitlab/project_template.rb,
Add details of the template in gitlab/ee/spec/lib/gitlab/project_template_spec.rb, in the enterprise_templates method, and
Add details of the template in gitlab/ee/app/assets/javascripts/projects/default_project_templates.js.
See MR !28187 for an example
Run the following in the gitlab project, where $name is the name you gave the template in gitlab/project_template.rb
bin/rake gitlab:update_project_templates[$name]
Run the bundle_repo script. Make sure to pass the correct arguments, or the script may damage the folder structure.
Add exported project ($name.tar.gz) to gitlab/vendor/project_templates and remove the resulting build folders tar-base and project
Run bin/rake gettext:regenerate in the gitlab project and commit new .pot file
Add changelog (e.g. bin/changelog -m 25486 "Add Project template for .NET")
Add icon to https://gitlab.com/gitlab-org/gitlab-svgs, as show in this example. If a logo is not available for the project, use the default 'Tanuki' logo instead.
Run yarn run svgs on gitlab-svgs project and commit result
Forward changes in gitlab-svgs project to master. This involves:
Merging your MR in gitlab-svgs
https://gitlab.com/gitlab-org/frontend/renovate-gitlab-bot/ will pick the new release up and create an MR in gitlab-org/gitlab
Once the bot-created MR created above is merged, you can rebase your template MR onto the updated master to pick up the new svgs
Test everything is working
Contributing an improvement to an existing template
Existing templates are available in the project-templates group.

To contribute a change, please open a merge request in the relevant project and mention @gitlab-org/manage/import/backend when you are ready for a review.

Then, if your merge request gets accepted, either open an issue on gitlab to ask for it to get updated, or open a merge request updating the vendored template using these instructions.
